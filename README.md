# PHP - Web scraping

Web scraping project looking for board games at i-szop.pl

## Technologies
Project is created with:

* **PHP 7.3**
* **fabpot/Goutte 3.2.3**
* **Guzzle 6.0** 
* **PHPmailer 6.0.7**


## Getting Started

Copy the project to the local machine. Then change sender_email and receiver_email in index.php.

## Run

The script can be started by 2 methods. 

### 1. Terminal
Uncomment the appropriate code and run index.php with 3 arguments "gameTitle", "maxGamePrice", "senderEmailPass". 
After a while you will receive an email with offers.

### 2. Browser

Uncomment the appropriate code and launch it in your browser. Arguments are defined as constants, but you can modify the code and enter a value in the URL ($_GET).
After a while you will receive an email with offers.