<?php
require 'vendor/autoload.php';

use Goutte\Client;   
use PHPMailer\PHPMailer\PHPMailer;

class GameSearch
{
    private $title;
    private $price;
    private $senderPass;
    private $receiverEmail;
    private $offers = [];
    private $url = "https://i-szop.pl/";
    private $searchURL;
    private $client;

    public function __construct($title, $price, $senderPass, $receiverEmail = "odakarol@gmail.com") // Change receiverEmail
    {
        $this->title = $title;
        $this->price = $price;
        $this->senderPass = $senderPass;
        $this->receiverEmail = $receiverEmail;
        $this->client = new Client();
    }

    private function getURL()
    {
        $crawler = $this->client->request('GET', $this->url);

        $form = $crawler->selectButton('searchButton')->form();
        $form['text'] = $this->title;

        $crawler = $this->client->submit($form);
        $uri = $form->getUri();
        $urlAfterSearch = $this->url . substr($uri,strpos($uri,'?'));

        $this->searchURL=$urlAfterSearch;
    }

    private function getLinks()
    {
        $crawler = $this->client->request('GET', $this->searchURL);
        $output = $crawler->filter('td.pad a')->extract('href');

        array_shift($output);
        array_shift($output);

        foreach($output as $key => $value){
            if($key & 1) 
            {
                unset($output[$key]);
            } 
            else 
            {
                $output[$key] = $this->url.$output[$key];
            }
        }
        
        $links = array_values($output);
        return $links;
    }
    
    private function getShops()
    {
        $crawler = $this->client->request('GET', $this->searchURL);
        $output = $crawler->filterXPath('//table')->filter('tr')->each(function ($tr, $i) { return $tr->filter('td')->each(function ($td, $i) { return trim($td->text()); }); }); 

        $output = array_diff_key($output, [0,1,2,3]);
        unset($output[6]);

        $output = array_values($output);
        foreach ($output as $element)
        {
            unset($element[0]);
            unset($element[2]);
            unset($element[3]);
            unset($element[6]);
            unset($element[7]);
            unset($element[8]);
            unset($element[9]);
            $shops[] = array_values($element);  
        }
        
        return $shops;
    }
    
    private function createOffersList()
    {
        $shops = $this->getShops();
        $links = $this->getLinks();   
        
        $numberShops = count($shops);
        $numberLinks = count($links);
        if($numberShops === $numberLinks)
        {
            for($i=0; $i < $numberShops; $i++)
            {
                $shops[$i][3] = $links[$i]; 
            }
        }
        
        $this->offers = $shops;
    }
    
    private function createMessage()
    {
        $textEmail = '';

        foreach($this->offers as $offer)
        {
            if($this->price > $offer[1])
            {
                $textEmail .= '<b>Sklep:</b> '.$offer[0].'<br>';
                $textEmail .= '<b>Cena:</b> '.$offer[1].'<br>';
                $textEmail .= '<b>Dostępność:</b> '.$offer[2].'<br>';
                $textEmail .= '<b>Link:</b> '.$offer[3].'<br><br>';
            }
        }

        if(empty($textEmail)) 
            $textEmail = "Niestety brak ofert spełniających warunek cenowy. Do usłyszenia jutro.<br><br>"; 

        return $textEmail;
    }
    
    private function sendEmail()
    {
        $textEmail = $this->createMessage();
        
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->CharSet = "UTF-8";
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->Username = 'KI.automat@gmail.com'; // Change Username
        $mail->Password = $this->senderPass;
        $mail->setFrom('KI.automat@gmail.com', 'KI AUTOMAT'); // Change senderEmail
        $mail->addReplyTo('KI.automat@gmail.com', 'KI AUTOMAT'); // Change senderEmail
        $mail->addAddress($this->receiverEmail, 'Szef');
        $mail->Subject = 'Lista sklepów z '. $this->title;
        $mail->msgHTML(file_get_contents('message.html'), __DIR__);
        $mail->Body = 'Znaleziono następujące sklepy: <br><br> '.$textEmail.'<br><br> Pozdrawiam,<br> Automat KI<br>';
        if (!$mail->send()) 
        {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } 
        else 
        {
            echo 'Message sent!';
        }
    } 
    
    public function init()
    {
        $this->getURL();
        $this->createOffersList();
        $this->sendEmail();
    }
}

// 1. Wywołanie przez konsole / Run by terminal
$gameTitle = $argv[1];
$gamePrice = $argv[2];
$emailPass = $argv[3];

try{
    if(!empty($argv[4]))
    {
        $receiverEmail = $argv[4];
    }
    else
    {
        throw new Exception('Nie podano adresu email odbiorcy. Skrypt użyje domyślnego adresu email.');
    }
} 
catch (Exception $ex) 
{
   echo $ex->getMessage();
}

if(empty($receiverEmail))
{
   $search = new GameSearch($gameTitle, $gamePrice, $emailPass);
}
else
{
   $search = new GameSearch($gameTitle, $gamePrice, $emailPass, $receiverEmail);    
}

// 2. Wywołanie przez przeglądarkę(localhost) - brak hasła / Run by browser
//  $search = new GameSearch('Splendor', 20, 'XXXXXXX'); // Set senderEmail password

 $search->init();